# -*- coding: utf-8 -*-
"""
Created on Sun Aug 15 14:23:04 2021

pytest guide:
https://semaphoreci.com/community/tutorials/testing-python-applications-with-pytest

@author: camar
"""

import pytest
import numpy as np


def test_capital_case():
    assert np.add(1, 1) == 2


def my_hello():
    return "Hello there"


if __name__ == '__main__':
    pytest.main()
